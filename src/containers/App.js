import React, { Component } from 'react';
import './App.css';
import Message from "../components/Message/Message";
import AddMessageForm from "../components/AddMessageForm/AddMessageForm";


let interval;

class App extends Component {

  state = {
    messages: [],
      text: '',
      author: '',
  };


  componentDidMount() {
      fetch('http://146.185.154.90:8000/messages').then(response => {
    if (response.ok) {
      return response.json();
    }

    throw new Error ('Sorry bro just error')

      }).then(messagesArray => {

          this.setState({messages: messagesArray});
      })

  }


  componentWillUnmount() {
      clearInterval(interval);
  }


    componentDidUpdate() {

      let date = this.state.messages[this.state.messages.length - 1].datetime;

      clearInterval(interval);

     interval = setInterval(() => {

          fetch('http://146.185.154.90:8000/messages?datetime=' + date).then(response => {
        if (response.ok) {
          return response.json();
        }
      }).then(message => {

          this.setState({messages: [...this.state.messages].concat(message) })


          })
    }, 5000)
  }



    SetText = event => {

    this.setState({text: event.target.value})

  };

    SetAuthor = event => {

        this.setState({author: event.target.value})

    };

  sendMessage = () => {

    const url = 'http://146.185.154.90:8000/messages';

    const data = new URLSearchParams();

    data.set('message', this.state.text);
    data.set('author', this.state.author);

    fetch(url, {
      method: 'post',
        body: data,
    }).then( post => {
        if (post.ok) {
          return post.json();
        }
    })

  };




    render() {
    return (
      <div className="App">
          <AddMessageForm
              addMessage={() => this.sendMessage()}
              onChangeText={(e) => this.SetText(e)}
              onChangeAuthor={(e) => this.SetAuthor(e)} />

                  <Message
                      messages={this.state.messages}
                  />
      </div>
    );
  }
}

export default App;
