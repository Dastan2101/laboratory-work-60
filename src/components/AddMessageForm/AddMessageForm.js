import React from 'react';
import './AddMessageForm Styles.css';


const AddMessageForm = (props) => {
    return (
        <div className="add-message-block">
            <input className="input form-control" type="text" onChange={props.onChangeText} value={props.text} placeholder="Enter some message"/>
            <input className="input form-control" type="text" onChange={props.onChangeAuthor} value={props.author} placeholder="Enter author name"/>
            <button className="add-btn btn btn-success" type="button" onClick={props.addMessage} >Add</button>
        </div>
    );
};

export default AddMessageForm;